package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

import magasin.Article;
import magasin.visiteur.PromoVisitor;


public class TestPromo extends TestCatalogueBase
{
    @Test
    public void testPromo()
    {
        List<Article> articles = super.catalogue.getArticles();
        ArrayList<Float> prixArticles = new ArrayList<>();

        for (Article article : articles) {
            float prix = article.getPrix();
            assertNotSame(0, prix);
            prixArticles.add(prix);
        }

        PromoVisitor promoVisitor = new PromoVisitor();
        promoVisitor.visit(super.catalogue);

        for (int i=0; i<articles.size(); ++i) {
            final float prixOriginal = prixArticles.get(i);
            final float prixPromo = articles.get(i).getPrix();
            assertEquals(prixOriginal-prixOriginal*10f/100f, prixPromo, 0.001f);
        }
    }
}
