package test;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.BeforeClass;

import magasin.Catalogue;
import magasin.fabrique.SimpleFabrique;


public abstract class TestCatalogueBase
{
    protected Catalogue catalogue;

    @BeforeClass
    public static void catalogueFileExists() {
        assertEquals(true, Files.exists(Paths.get("catalogue.txt")));
    }

    @Before
    public void initCatalogue() {
        this.catalogue = new Catalogue();
        SimpleFabrique fabrique = new SimpleFabrique();
        Logger log = Logger.getLogger("log");
		String ligne;
		try (BufferedReader lecteurAvecBuffer = Files.newBufferedReader(Paths.get("catalogue.txt"), StandardCharsets.UTF_8)) {
			while ((ligne = lecteurAvecBuffer.readLine()) != null)
				catalogue.addProd(fabrique.creerProduit(ligne));
		} catch (Exception exc) {
			log.log(Level.SEVERE, "Erreur à la lecture du fichier de stock {0}: ", exc.getMessage());
		}
        assertEquals(false, this.catalogue.isEmpty());
    }
}
