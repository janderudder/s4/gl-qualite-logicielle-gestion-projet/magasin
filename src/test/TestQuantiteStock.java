package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;

import java.util.List;
import org.junit.Test;

import magasin.Article;


public class TestQuantiteStock extends TestCatalogueBase
{
    @Test
    public void testQuantite()
    {
        List<Article> articles = super.catalogue.getArticles();

        for (Article article : articles) {
            int quantite = article.getQuantite();
            assertNotSame(0, quantite);
            try {
                article.vendre(1);
                assertEquals(quantite-1, article.getQuantite());
            } catch (Exception e) {
                fail(e.getMessage());
            }
        }
    }
}
