package test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import magasin.visiteur.PromoVisitor;
import magasin.visiteur.ValeurStockVisitor;


public class TestValeurStock extends TestCatalogueBase
{
    @Test
    public void testPromoStock()
    {
        ValeurStockVisitor valeurStockVisitor = new ValeurStockVisitor();
        valeurStockVisitor.visit(catalogue);
        final float valeurStock = valeurStockVisitor.getStockValue();

        PromoVisitor promoVisitor = new PromoVisitor();
        promoVisitor.visit(catalogue);

        valeurStockVisitor.visit(catalogue);
        assertEquals(valeurStock-valeurStock*10f/100f, valeurStockVisitor.getStockValue(), 0.001f);
    }
}
