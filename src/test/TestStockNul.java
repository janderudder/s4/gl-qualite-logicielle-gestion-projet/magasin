package test;

import static org.junit.Assert.assertThrows;

import org.junit.Test;

import magasin.Article;
import magasin.ExceptionStockNul;


public class TestStockNul extends TestCatalogueBase
{
    @Test
    public void testVenteStockNul()
    {
        Article article = super.catalogue.getArticles().get(0);

        int quantite = article.getQuantite();

        assertThrows(ExceptionStockNul.class, () -> article.vendre(quantite+1));
    }
}
