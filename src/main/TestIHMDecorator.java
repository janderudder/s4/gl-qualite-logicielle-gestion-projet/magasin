package main;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import magasin.Catalogue;
import magasin.fabrique.SimpleFabrique;
import magasin.ihm.*;



public class TestIHMDecorator
{
    /**
     * Contrôleur pour l'affichage du menu avec le pattern Decorator.
     * Dans une version de production les actions seraient globales au niveau
     * de l'application et leur égalité serait testée via leur référence ou un id.
     */
    public static void main(String[] args)
    {
        SimpleFabrique simpleFabrique = new SimpleFabrique();
		Catalogue catalogue = new Catalogue();
		Logger log = Logger.getLogger("log");
		Path path = Paths.get("catalogue.txt");
		String ligne;

		try (BufferedReader lecteurAvecBuffer = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
			while ((ligne = lecteurAvecBuffer.readLine()) != null)
				catalogue.addProd(simpleFabrique.creerProduit(ligne));
		} catch (Exception exc) {
			log.log(Level.SEVERE, "Erreur à la lecture du fichier de stock {0}: ", exc.getMessage());
		}


        ICatalogueIHM ihm = new CatalogueIHM(catalogue);

        Stack<ICatalogueIHM> menuStack = new Stack<>();

        menuStack.push(new DecoIHMChoixUtilisateur(ihm));

        while (!menuStack.isEmpty())
        {
            final String option = menuStack.lastElement().afficherMenu();

            switch (option) {
                case "Quitter":
                case "Retour":
                    menuStack.pop();
                break;

                case "Menu Administrateur":
                    menuStack.push(new DecoIHMAdmininistrateur(new DecoIHMUtilisateur(ihm)));
                break;

                case "Menu Gestionnaire":
                    menuStack.push(new DecoIHMGestionnaire(new DecoIHMUtilisateur(ihm)));
                break;

                case "Menu Vendeur":
                    menuStack.push(new DecoIHMVendeur(new DecoIHMUtilisateur(ihm)));
                break;

                case "Afficher le catalogue":
                    System.out.println(catalogue);
                break;

                case "Exporter en HTML":
                    // TODO: enregistrer le catalogue au format HTML
                break;

                default:
            }
        }
    }
}
