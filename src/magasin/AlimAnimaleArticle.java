package magasin;


public class AlimAnimaleArticle extends Article
{
    private String animal;
    private float  masse;

    public AlimAnimaleArticle(
        String nom,
        String identifiant,
        float prix,
        int quantite,
        String animal,
        float masse
    ){
        super(nom, identifiant, prix, quantite);
        this.animal = animal;
        this.masse = masse;
    }

    @Override
    public String toString() {
        return "AlimAnimalArticle {"
            + super.toString()
            + ", animal: \"" + this.animal + "\""
            + ", masse: " + this.masse
            + "}";
    }
}
