package magasin;


public class JardiArticle extends Article
{
    public enum Saison
    {
        PRINTEMPS,
        ETE,
        AUTOMNE,
        HIVER
    }

    private Saison saison;

    public JardiArticle(
        String nom,
        String identifiant,
        float prix,
        int quantite,
        Saison saison
    ){
        super(nom, identifiant, prix, quantite);
        this.saison = saison;
    }

    @Override
    public String toString()
    {
        return "JardiArticle {"
            + super.toString()
            + ", saison: \"" + this.saison + "\""
            + "}";
    }
}
