package magasin.ihm;
import java.util.ArrayList;
import magasin.Catalogue;


public interface ICatalogueIHM
{
    public Catalogue getCatalogue();
    public ArrayList<String> getOptions();
    public String afficherMenu();
}
