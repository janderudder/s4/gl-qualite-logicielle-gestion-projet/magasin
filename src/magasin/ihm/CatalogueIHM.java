package magasin.ihm;
import java.util.ArrayList;
import java.util.Scanner;
import magasin.Catalogue;


public class CatalogueIHM implements ICatalogueIHM
{
    private ArrayList<String> options;
    protected Catalogue catalogue;

    public CatalogueIHM(Catalogue catalogue)
    {
        this.options = new ArrayList<>();
        this.options.add("Quitter");
        this.catalogue = catalogue;
    }

    @Override
    public Catalogue getCatalogue() {
        return this.catalogue;
    }

    @Override
    public ArrayList<String> getOptions()
    {
        return new ArrayList<>(this.options);
    }

    @Override
    public String afficherMenu()
    {
        System.out.println("\nMenu");
        System.out.println("------------------------");

        int choice = -1;
        ArrayList<String> options = this.getOptions();
        Scanner scan = new Scanner(System.in);

        while (choice<0 || choice>=options.size()) {
            for (int i = 0; i< options.size(); ++i) {
                System.out.println("("+i+") "+options.get(i));
            }
            System.out.println();
            try {
                choice = scan.nextInt();
                System.out.println("\n>> "+options.get(choice));
            }
            catch (Exception e) { scan.skip(".*"); }
        }
        return options.get(choice);
    }
}
