package magasin.ihm;
import java.util.ArrayList;


public class DecoIHMUtilisateur extends DecorateurIHM
{
    public DecoIHMUtilisateur(ICatalogueIHM ihm) {
        super(ihm, "Utilisateur");
    }

    @Override
    public ArrayList<String> getOptions()
    {
        ArrayList<String> options = this.ihm.getOptions();
        if (options.contains("Quitter")) {
            options.set(options.indexOf("Quitter"), "Retour");
        }
        options.add("Afficher le catalogue");
        return options;
    }
}
