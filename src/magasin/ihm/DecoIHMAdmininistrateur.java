package magasin.ihm;
import java.util.ArrayList;


public class DecoIHMAdmininistrateur extends DecorateurIHM
{
    public DecoIHMAdmininistrateur(ICatalogueIHM ihm) {
        super(ihm, "Administrateur");
    }

    @Override
    public ArrayList<String> getOptions()
    {
        ArrayList<String> options = this.ihm.getOptions();
        if (options.contains("Quitter")) {
            options.set(options.indexOf("Quitter"), "Retour");
        }
        options.add("Appliquer promotion flash");
        return options;
    }
}
