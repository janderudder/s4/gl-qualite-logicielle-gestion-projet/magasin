package magasin.ihm;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import magasin.Article;


public class DecoIHMVendeur extends DecorateurIHM
{
    public DecoIHMVendeur(ICatalogueIHM ihm) {
        super(ihm, "Vendeur");
    }

    @Override
    public ArrayList<String> getOptions()
    {
        ArrayList<String> options = this.ihm.getOptions();
        if (options.contains("Quitter")) {
            options.set(options.indexOf("Quitter"), "Retour");
        }
        options.add("Vendre un produit");
        return options;
    }

    @Override
    public String afficherMenu()
    {
        final String option = super.afficherMenu();

        if (option.equals("Vendre un produit")) {
            this.afficherSousMenu();
        }
        return option;
    }

    public void afficherSousMenu()
    {
        int choice = -1;
        try (Scanner scan = new Scanner(System.in))
        {
            while (choice!=0)
            {
                System.out.println("\nVendre un produit");
                System.out.println("------------------------");

                System.out.println("(0) Retour");

                List<Article> articles = this.ihm.getCatalogue().getArticles();

                final int articleCount = articles.size();

                for (int i=0; i < articleCount; ++i)
                {
                    Article article = articles.get(i);
                    System.out.println("("+(i+1)+") Vendre "+article.getNom()+" ("+article.getIdentifiant()+")");
                }

                System.out.print("\nVotre choix ? ");

                try {
                    choice = scan.nextInt();
                    System.out.println("\n>> ("+choice+")\n");

                    if (choice>=1 && choice <= articleCount)
                    {
                        Article article = articles.get(choice-1);
                        final int quantite = article.getQuantite() - 1;

                        if (quantite > -1) {
                            System.out.println("Un article \""+article.getNom()+"\" a été vendu.");
                            System.out.println("Quantité restante: "+quantite+" unité(s).\n");
                            article.setQuantite(quantite);
                        } else {
                            System.out.println("Il n'y a plus d'article \""+article.getNom()+"\", vente impossible.");
                        }
                    }
                }
                catch (Exception e) { scan.skip(".*"); }
            }
        }
    }
}
