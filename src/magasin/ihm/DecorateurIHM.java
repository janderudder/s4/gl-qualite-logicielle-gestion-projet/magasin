package magasin.ihm;
import java.util.ArrayList;
import java.util.Scanner;
import magasin.Catalogue;


public abstract class DecorateurIHM implements ICatalogueIHM
{
    protected ICatalogueIHM ihm;
    private final String titre;

    public DecorateurIHM(ICatalogueIHM ihm, String titre) {
        this.ihm = ihm;
        this.titre = titre;
    }

    @Override
    public Catalogue getCatalogue() {
        return this.ihm.getCatalogue();
    }

    @Override
    public abstract ArrayList<String> getOptions();

    public final String getTitre() {
        return this.titre;
    }

    @Override
    public String afficherMenu()
    {
        int choice = -1;
        ArrayList<String> options = this.getOptions();
        Scanner scan = new Scanner(System.in);

        while (choice<0 || choice>=options.size())
        {
            System.out.println("\nMenu " + this.getTitre());
            System.out.println("------------------------");
            for (int i = 0; i< options.size(); ++i) {
                System.out.println("("+i+") "+options.get(i));
            }
            System.out.print("\nVotre choix ? ");
            try {
                choice = scan.nextInt();
                System.out.println("\n>> "+options.get(choice));
            }
            catch (Exception e) { scan.skip(".*"); }
        }
        return options.get(choice);
    }
}
