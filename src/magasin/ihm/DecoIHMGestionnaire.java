package magasin.ihm;
import java.util.ArrayList;


public class DecoIHMGestionnaire extends DecorateurIHM
{
    public DecoIHMGestionnaire(ICatalogueIHM ihm) {
        super(ihm, "Gestionnaire du Stock");
    }

    @Override
    public ArrayList<String> getOptions()
    {
        ArrayList<String> options = this.ihm.getOptions();
        if (options.contains("Quitter")) {
            options.set(options.indexOf("Quitter"), "Retour");
        }
        options.add("Calculer valeur stock");
        options.add("Exporter en HTML");
        return options;
    }
}
