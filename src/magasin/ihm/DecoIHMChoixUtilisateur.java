package magasin.ihm;
import java.util.ArrayList;


public class DecoIHMChoixUtilisateur extends DecorateurIHM
{
    public DecoIHMChoixUtilisateur(ICatalogueIHM ihm) {
        super(ihm, "Choix Utilisateur");
    }

    @Override
    public ArrayList<String> getOptions()
    {
        ArrayList<String> options = this.ihm.getOptions();
        options.add("Menu Administrateur");
        options.add("Menu Gestionnaire");
        options.add("Menu Vendeur");
        return options;
    }
}
