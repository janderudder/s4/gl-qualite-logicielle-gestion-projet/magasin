package magasin.visiteur;


public interface Visitable
{
    public void accept(Visitor visitor);
}
