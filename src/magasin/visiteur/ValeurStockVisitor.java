package magasin.visiteur;
import java.util.logging.Level;
import java.util.logging.Logger;
import magasin.Article;
import magasin.Catalogue;


public class ValeurStockVisitor implements Visitor
{
    float stockValue = 0.f;

    @Override
    public void visit(Visitable catalogue)
    {
        this.stockValue = 0.f;

        if (catalogue instanceof Catalogue)
        {
            for (Article article : ((Catalogue)catalogue).getArticles()) {
                this.stockValue += article.getPrix() * article.getQuantite();
            }
            Logger log = Logger.getLogger("log");
            log.log(Level.INFO, "La valeur totale du stock est: {0} €.", this.stockValue);
        }
    }

    public final float getStockValue() {
        return stockValue;
    }
}
