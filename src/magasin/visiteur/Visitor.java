package magasin.visiteur;

/**
 * This variant of the Visitor pattern uses type matching.
 * Each visit method implementation is responsible for checking the type.
 */
public interface Visitor
{
    public void visit(Visitable visitable);
}
