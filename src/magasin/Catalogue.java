package magasin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import magasin.observateur.Message;
import magasin.observateur.Observable;
import magasin.observateur.Observer;
import magasin.visiteur.Visitable;
import magasin.visiteur.Visitor;


public class Catalogue implements Visitable, Observer
{
    private ArrayList<Article> articles = new ArrayList<>();

    public List<Article> getArticles()
    {
        return this.articles;
    }

    public void addProd(Article article)
    {
        this.articles.add(article);
        article.addObserver(this);
    }

    @Override
    public void accept(Visitor visitor)
    {
        visitor.visit(this);
    }

    @Override
    public void update(Observable obs, Message message)
    {
        Logger log = Logger.getLogger("log");

        final String nom = ((Article)obs).getNom();
        String eventString = "";

        switch (message.property) {
            case "prix":
                eventString = "de prix: \""+nom+"\" passe à "+(float)(message.value)+"€";
            break;
            case "quantité":
                final int qte = (int)message.value;
                eventString = "de quantité: le stocke de \""+nom+"\" passe à "+qte+" unité"+(qte>1?"s":"");
            break;
            default:
                eventString = "de type <"+message.property+">";
        }

        log.log(Level.INFO, "<Catalogue.update> Enregistrement du catalogue suite à un changement {0}", eventString);
    }

    public boolean isEmpty() {
        return this.articles.isEmpty();
    }

    public int size() {
        return this.articles.size();
    }

    @Override
    public String toString()
    {
        StringBuilder strBuilder = new StringBuilder();

        for (Article article : this.articles) {
            strBuilder.append(article);
            strBuilder.append(";\n");
        }

        return strBuilder.toString();
    }
}
