package magasin.state;

import magasin.ihm.DecoIHMChoixUtilisateur;
import magasin.ihm.ICatalogueIHM;


public class StateChoixUtil extends IHMState
{
    private ICatalogueIHM ihm;

    public StateChoixUtil(ICatalogueIHM baseIhm)
    {
        super(baseIhm);
        this.ihm = new DecoIHMChoixUtilisateur(this.baseIhm);
    }

    @Override
    public ICatalogueIHM ihm() {
        return this.ihm;
    }

    @Override
    public IHMState interact()
    {
        final String option = this.ihm.afficherMenu();

        switch (option) {
            case "Quitter":
                return new StateEnd(this.baseIhm());

            case "Menu Administrateur":
                return new StateAdministrateur(this.baseIhm());

            case "Menu Gestionnaire":
                return new StateGestionnaire(this.baseIhm());

            case "Menu Vendeur":
                return new StateVendeur(this.baseIhm());

            default:
                return this;
        }
    }
}
