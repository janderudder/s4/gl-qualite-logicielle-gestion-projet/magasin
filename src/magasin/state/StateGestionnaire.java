package magasin.state;

import magasin.ihm.DecoIHMGestionnaire;
import magasin.ihm.DecoIHMUtilisateur;
import magasin.ihm.ICatalogueIHM;


public class StateGestionnaire extends IHMState
{
    private ICatalogueIHM ihm;

    public StateGestionnaire(ICatalogueIHM baseIhm)
    {
        super(baseIhm);
        this.ihm = new DecoIHMGestionnaire(new DecoIHMUtilisateur(this.baseIhm));
    }

    @Override
    public ICatalogueIHM ihm() {
        return this.ihm;
    }

    @Override
    public IHMState interact()
    {
        final String option = ihm.afficherMenu();

        switch (option) {
            case "Retour":
                return new StateChoixUtil(this.baseIhm());

            default:
                return this;
        }
    }
}
