package magasin.state;
import magasin.ihm.ICatalogueIHM;


public interface IState
{
    public ICatalogueIHM baseIhm();
    public ICatalogueIHM ihm();
    public IHMState interact();
}
