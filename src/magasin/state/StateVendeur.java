package magasin.state;

import magasin.ihm.DecoIHMUtilisateur;
import magasin.ihm.DecoIHMVendeur;
import magasin.ihm.ICatalogueIHM;


public class StateVendeur extends IHMState
{
    private ICatalogueIHM ihm;

    public StateVendeur(ICatalogueIHM baseIhm)
    {
        super(baseIhm);
        this.ihm = new DecoIHMVendeur(new DecoIHMUtilisateur(this.baseIhm));
    }

    @Override
    public ICatalogueIHM ihm() {
        return this.ihm;
    }

    @Override
    public IHMState interact()
    {
        final String option = ihm.afficherMenu();

        switch (option) {
            case "Retour":
                return new StateChoixUtil(this.baseIhm());

            default:
                return this;
        }
    }
}
