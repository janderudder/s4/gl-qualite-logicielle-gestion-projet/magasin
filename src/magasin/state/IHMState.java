package magasin.state;
import magasin.ihm.ICatalogueIHM;


public abstract class IHMState implements IState
{
    protected ICatalogueIHM baseIhm;

    public IHMState(ICatalogueIHM baseIhm) {
        this.baseIhm = baseIhm;
    }

    @Override
    public ICatalogueIHM baseIhm() {
        return this.baseIhm;
    }

    @Override
    public abstract ICatalogueIHM ihm();

    @Override
    public abstract IHMState interact();
}
