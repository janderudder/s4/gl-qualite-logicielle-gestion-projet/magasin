package magasin.state;

import magasin.ihm.ICatalogueIHM;


public class StateEnd extends IHMState
{
    public StateEnd(ICatalogueIHM baseIhm)
    {
        super(baseIhm);
    }

    @Override
    public ICatalogueIHM ihm() {
        return null;
    }

    @Override
    public IHMState interact()
    {
        return null;
    }
}
