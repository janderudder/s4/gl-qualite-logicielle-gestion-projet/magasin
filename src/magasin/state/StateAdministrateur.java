package magasin.state;

import magasin.ihm.DecoIHMAdmininistrateur;
import magasin.ihm.DecoIHMUtilisateur;
import magasin.ihm.ICatalogueIHM;


public class StateAdministrateur extends IHMState
{
    private ICatalogueIHM ihm;

    public StateAdministrateur(ICatalogueIHM baseIhm)
    {
        super(baseIhm);
        this.ihm = new DecoIHMAdmininistrateur(new DecoIHMUtilisateur(this.baseIhm()));
    }

    @Override
    public ICatalogueIHM ihm() {
        return this.ihm;
    }

    @Override
    public IHMState interact()
    {
        final String option = ihm.afficherMenu();

        switch (option) {
            case "Retour":
                return new StateChoixUtil(this.baseIhm());
        }
        return null;
    }
}
