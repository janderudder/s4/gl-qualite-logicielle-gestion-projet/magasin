package magasin.observateur;

public class Message
{
    public final String property;
    public final Object value;

    public Message(String property, Object value)
    {
        this.property = property;
        this.value = value;
    }
}
