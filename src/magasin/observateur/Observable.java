package magasin.observateur;
import java.util.HashSet;
import magasin.observateur.Observer;


public abstract class Observable
{
    private HashSet<Observer> observers = new HashSet<>();

    public boolean addObserver(Observer observer)
    {
        return this.observers.add(observer);
    }

    public boolean removeObserver(Observer observer)
    {
        return this.observers.remove(observer);
    }

    public void notifyObserver(String property, Object value) {
        for (Observer obs : this.observers) {
            obs.update(this, new Message(property, value));
        }
    }
}
