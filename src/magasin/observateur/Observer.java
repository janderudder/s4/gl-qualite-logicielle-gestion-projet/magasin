package magasin.observateur;

public interface Observer
{
    public void update(Observable obs, Message message);
}
