package magasin;
import magasin.observateur.Observable;
import magasin.visiteur.Visitable;
import magasin.visiteur.Visitor;


public abstract class Article
    extends    Observable
    implements Visitable
{
    protected String nom;
    protected String identifiant;
    protected float prix;
    protected int quantite;

    protected Article(String nom, String identifiant, float prix, int quantite)
    {
        this.nom = nom;
        this.identifiant = identifiant;
        this.prix = prix;
        this.quantite = quantite;
    }

    public String getNom() {
        return this.nom;
    }

    public float getPrix() {
        return this.prix;
    }

    public int getQuantite() {
        return this.quantite;
    }

    public String getIdentifiant() {
        return this.identifiant;
    }

    public void setPrix(float prix)
    {
        this.prix = prix;
        this.notifyObserver("prix", prix);
    }

    public void setQuantite(int qte)
    {
        this.quantite = qte;
        this.notifyObserver("quantité", qte);
    }

    public void vendre(int qte) throws ExceptionStockNul
    {
        if (qte<=this.quantite) {
            this.setQuantite(this.quantite - qte);
        } else {
            throw new ExceptionStockNul("pas assez d'articles en stock pour la vente");
        }
    }

    @Override
    public void accept(Visitor visitor)
    {
        visitor.visit(this);
    }

    @Override
    public String toString()
    {
        return "nom: \"" + this.nom + "\", "
            + "identifiant: \"" + this.identifiant + "\", "
            + "prix: " + this.prix + "€, "
            + "quantité: " + this.quantite;
    }
}
