package magasin.fabrique;

import java.util.Arrays;
import java.util.List;
import magasin.AlimAnimaleArticle;
import magasin.Article;
import magasin.BricoArticle;
import magasin.JardiArticle;


public class SimpleFabrique
{
    public Article creerProduit(String entreeCatalogue)
    {
        final List<String> arguments = Arrays.asList(entreeCatalogue.split(";"));

        final String categorie = arguments.get(0)
            .trim()
            .toLowerCase();

        final String nom = arguments.get(3);
        final String identifiant = arguments.get(1);
        final float prix = Float.parseFloat(arguments.get(4).trim());
        final int quantite = Integer.parseInt(arguments.get(2).trim());

        if (categorie.equals("jardinage"))
        {
            final JardiArticle.Saison saison =
                JardiArticle.Saison.valueOf(arguments.get(5).toUpperCase());
            return new JardiArticle(nom, identifiant, prix, quantite, saison);
        }
        else if (categorie.equals("bricolage"))
        {
            final String rayon = arguments.get(5);
            final boolean estElectrique =
                arguments.get(6).equalsIgnoreCase("nonelectrique");
            return new BricoArticle(nom, identifiant, prix, quantite, rayon, estElectrique);
        }
        else if (categorie.equals("animal"))
        {
            final String animal = arguments.get(5);
            final float masse = Float.parseFloat(arguments.get(6));
            return new AlimAnimaleArticle(nom, identifiant, prix, quantite, animal, masse);
        }
        return null; // shouldn't reach here in the exercise
    }
}
