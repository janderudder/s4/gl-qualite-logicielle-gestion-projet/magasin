package magasin;


public class BricoArticle extends Article
{
    private String rayon;
    private boolean estElectrique;

    public BricoArticle(
        String nom,
        String identifiant,
        float prix,
        int quantite,
        String rayon,
        boolean estElectrique
    ){
        super(nom, identifiant, prix, quantite);
        this.rayon = rayon;
        this.estElectrique = estElectrique;
    }

    @Override
    public String toString()
    {
        return "BricoArticle {"
            + super.toString()
            + ", rayon: \"" + this.rayon + "\", "
            + "estÉlectrique: " + this.estElectrique
            + "}";
    }
}
